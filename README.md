# astrel-shell

My custom [Oh-My-Posh](https://github.com/jandedobbeleer/oh-my-posh) shell theme.

![Generated with `oh-my-posh config export image --cursor-padding 50 --rprompt-offset 4 --author ASTRELION`][png]

**Features**

*Things I wanted that I couldn't find in other themes*

- Full User/Host/Path URI
    - Useful for copy/pasting, SSH-ing, etc.
- Simple but complete Git information
    - Current branch name
    - Commit diff, in green (e.g. `▴1 ▾2`)
    - Stage diff, in yellow (e.g. `+1 -2 ~3`)
    - Working diff, in red (e.g. `+1 -2 ~3`)
    - Staged changes, in grey ($)
- Left bracket turns red if the last command returned an error code
- Multiline support
- "Plain"text (doesn't need a [Nerdfont](https://github.com/ryanoasis/nerd-fonts))
    - Utilizes some standard Unicode symbols
- 2 lines (including input line)
- TOML because JSON is ugly :)

## Install

To use this theme, [install Oh-My-Posh](https://ohmyposh.dev/docs/installation/linux) first. Then, follow the directions below for your shell or [look here](https://ohmyposh.dev/docs/installation/customize#config-syntax).

### Bash

1. Download [astrel-shell.toml][toml]
2. Open Bash profile (e.g. `nano ~/.bash_profile`)
3. Add the following line:
```bash
eval "$(oh-my-posh init bash --config /path/to/file/astrel-shell.toml)"
```

### PowerShell

1. Download [astrel-shell.toml][toml]
2. Open PowerShell profile (e.g. `code $PROFILE`)
3. Add the following line:
```powershell
oh-my-posh init pwsh --config 'C:\path\to\file\astrel-shell.toml' | Invoke-Expression
```

## Configuration

You can change colors via the "palette" section in [astrel-shell.toml][toml].

[toml]: ./astrel-shell.toml
[png]: ./astrel-shell.png
