"$schema" = "https://raw.githubusercontent.com/JanDeDobbeleer/oh-my-posh/main/themes/schema.json"

version = 2
console_title_template = "{{ .UserName }}@{{ .HostName }}:{{ .PWD }}"

[palette]
accent          = "#444444" # dark grey
uri             = "#ee44ee" # pink
uri-accent      = "#dd99dd" # light pink
path            = "#44ccff" # blue
git             = "#888888" # light grey
commit-diff     = "#c8fac8" # green
stage-diff      = "#fafac8" # yellow
working-diff    = "#fac8c8" # red
stash           = "#666666" # grey
error           = "#a83838" # red

# line 0 - user, host, and path
[[blocks]]
type = "prompt"
alignment = "left"
overflow = "break"

    # L
    [[blocks.segments]]
    type = "text"
    style = "plain"
    template = "\u256d"
    foreground = "p:accent"
    foreground_templates = [
        "{{ if ne .Code 0 }}p:error{{ end }}"
    ]

    # User
    [[blocks.segments]]
    type = "text"
    style = "plain"
    template = "{{ .UserName }}"
    foreground = "p:uri"

    # @
    [[blocks.segments]]
    type = "text"
    style = "plain"
    template = "@"
    foreground = "p:uri-accent"

    # Hostname
    [[blocks.segments]]
    type = "text"
    style = "plain"
    template = "{{ .HostName }}"
    foreground = "p:uri"

    # :
    [[blocks.segments]]
    type = "text"
    style = "plain"
    template = ":"
    foreground = "p:accent"

    # Working directory
    [[blocks.segments]]
    type = "path"
    style = "plain"
    template = "{{ .PWD }}\u00A0"
    foreground = "p:path"

        [blocks.segments.properties]
        style = "folder"

    [[blocks.segments]]
    type = "python"
    style = "plain"
    template = "{{ if .Venv }}venv {{ .Full }} {{ end }}"

# line 0 - Git information
[[blocks]]
type = "prompt"
alignment = "right"
overflow = "break"
filler = "<#444444>-</>"

    [[blocks.segments]]
    type = "text"
    style = "plain"
    template = "\u00A0"

    # Git branch
    [[blocks.segments]]
    type = "git"
    style = "plain"
    foreground = "p:git"
    template = "{{ url .HEAD .UpstreamURL }}"

        [blocks.segments.properties]
        fetch_status = true
        fetch_worktree_count = true
        branch_icon = ""
        branch_idential_icon = ""
        branch_ahead_icon = ""
        branch_behind_icon = ""
        branch_gone_icon = ""
        branch_max_length = ""
        commit_icon = ""
        tag_icon = ""
        rebase_icon = ""
        cherry_pick_icon = ""
        revert_icon = ""
        merge_icon = ""
        no_commits_icon = ""

    # :
    [[blocks.segments]]
    type = "git"
    style = "plain"
    foreground = "p:accent"
    template = "{{ if or (.Ahead) (.Behind) }}:{{ end }}"

        [blocks.segments.properties]
        fetch_status = true

    # Commits vs upstream (green)
    # ^<commits ahead of upstream> \/<commits behind upstream>
    [[blocks.segments]]
    type = "git"
    style = "plain"
    foreground = "p:commit-diff"
    template = "{{ if .Ahead }}\u25b4{{ .Ahead }}{{ end }}{{ if and (.Ahead) (.Behind) }} {{ end }}{{ if .Behind }}\u25be{{ .Behind }}{{ end }}"

        [blocks.segments.properties]
        fetch_status = true

    # :
    [[blocks.segments]]
    type = "git"
    style = "plain"
    foreground = "p:accent"
    template = "{{ if .Staging.Changed }}:{{ end }}"

        [blocks.segments.properties]
        fetch_status = true

    # Staged changes (yellow)
    [[blocks.segments]]
    type = "git"
    style = "plain"
    foreground = "p:stage-diff"
    template = "{{ if .Staging.Changed }}{{ .Staging.String }}{{ end }}"

        [blocks.segments.properties]
        fetch_status = true

    # :
    [[blocks.segments]]
    type = "git"
    style = "plain"
    foreground = "p:accent"
    template = "{{ if .Working.Changed }}:{{ end }}"

        [blocks.segments.properties]
        fetch_status = true

    # Working changes (red)
    [[blocks.segments]]
    type = "git"
    style = "plain"
    foreground = "p:working-diff"
    template = "{{ if .Working.Changed }}{{ .Working.String }}{{ end }}"

        [blocks.segments.properties]
        fetch_status = true

    # :
    [[blocks.segments]]
    type = "git"
    style = "plain"
    foreground = "p:accent"
    template = "{{ if gt .StashCount 0 }}:{{ end }}"

        [blocks.segments.properties]
        fetch_status = true

    # Stashed changes (grey)
    [[blocks.segments]]
    type = "git"
    style = "plain"
    foreground = "p:stash"
    template = "{{ if gt .StashCount 0 }}${{ end }}"

        [blocks.segments.properties]
        fetch_status = true

# line 1
[[blocks]]
alignment = "left"
type = "prompt"
newline = true

    # L
    [[blocks.segments]]
    type = "text"
    style = "plain"
    template = "\u2570"
    foreground = "p:accent"
    foreground_templates = [
        "{{ if ne .Code 0 }}p:error{{ end }}"
    ]

    # $
    [[blocks.segments]]
    type = "text"
    style = "plain"
    template = "$ "
    foreground = "p:accent"

# multiline
[secondary_prompt]
foreground = "p:accent"
template = " > "
